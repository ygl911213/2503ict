<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Social Network</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link href="css/style.css" rel="stylesheet" type="text/css" />

    <!-- Custom styles for this template -->
    <link href="css/styles.css" rel="stylesheet">
 {{ HTML::style('css/styles.css'); }}

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </head>

  <body>

   <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
           
                 </div>
               </button>
            <a class="navbar-brand" href="http://demo-project-ygl911213-2.c9.io/2503ict-assign1/assoc-laravel/public/post">Social Network</a>
     
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="http://demo-project-ygl911213-2.c9.io/2503ict-assign1/assoc-laravel/public/post">home</a></li>
              <li><a href="http://demo-project-ygl911213-2.c9.io/2503ict-assign1/assoc-laravel/public/comment">information</a></li>
          
          <li><a href= "https://demo-project-ygl911213-2.c9.io/2503ict-assign1/assoc-laravel/public/user/create">sign up</a></li>
             @if (Auth::check()) 
    <li> {{ link_to_route('user.logout', 'Logout') }}</li>
    @else
           <li><a href= "https://demo-project-ygl911213-2.c9.io/2503ict-assign1/assoc-laravel/public/user">login</a></li>
         @endif  
           
   <div id="tfheader">
     
     
    {{ Form::open(array('method' => 'GET', 'route' =>'friend.search','class'=> 'input-group')) }}
		      
   {{ Form::text('friendname') }}
		      {{ Form::submit('search') }}
		      
		{{ Form::close() }}
	<div class="tfclear"></div>
	</div>
	
            </ul>
            
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
      
@section('title')
@show
      <!-- Main body -->
      
      <div class= 'row'>
          <div class='col-md-4'>
            
             @section('content')
             @show
          </div>
          <div class='col-md-8'>
            @section('post')
            @show
          </div>
      </div>
     </div>

  </body>
</html>
 