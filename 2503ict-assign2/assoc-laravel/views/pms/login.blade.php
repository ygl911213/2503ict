@extends('layouts.master')

<!--login form-->


@section('content')
{{ Session::get('login_failed') }}
   
 @if (Auth::check()) 
    {{ Auth::user()->username }} 
    {{ Auth::user()->birth }}

   
 @else

    {{ Form::open(array('url' => secure_url('user/login'))) }} 
{{ Form::label('username', 'User Name: ') }}
{{ Form::text('username') }}

{{ Form::label('password', 'Password: ') }}
{{ Form::password('password') }}

{{ Form::submit('Sign in') }}
{{ Form::close() }}

@endif
@stop

