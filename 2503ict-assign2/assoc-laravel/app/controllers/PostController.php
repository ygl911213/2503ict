<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		


$num_items = Post::query()->count();

$total_pages = ceil($num_items / 8);
		if (Auth::check()) {
			
	

$Users =User::where('id', '=', Auth::user()->id)->get();
$postid =Post::where('name','=', Auth::user()->username)->orWhere('privacy_public','=','public')->orderBy('id','DESC')->get();


     return View::make('pms.home')->with('users',$Users)->with('postid',$postid)->with('total',$total_pages);
   
    
		}
		
		else{
	   $Posts = Post::where('privacy_public','=','public')->orderBy('id','DESC')->get();
	  
        return View::make('pms.home')->with('pms', $Posts)->with('total',$total_pages);
        
     
        
		}
        
        
        
		}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
if (Auth::check()) {		

 $input = Input::all();
 
 $post = new Post();
 $post->name = Auth::user()->username;
 $post->title = $input['title'];
 $post->message = $input['message'];
  $post->privacy_public = $input['privacy_public'];
 $post->save();
return Redirect::route('post.index', $post->id);
}

else {
	
	return Redirect::route('user.index');
	
}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
			$posts = Post::find($id);
			

			

 		
 return View::make('pms.edit')->with('pms', $posts);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
$input = Input::all();
//$v = Validator::make($input, post::$rules);
 //if ($v->passes())
 //{
		$post = Post::find($id);
 
 //$post->name = $input['name'];
// $post->title = $input['title'];
 $post->message = $input['message'];
 $post->privacy_public = $input['privacy_public'];
 $post->save();
 return Redirect::route('post.index', $post->id);

//	}

}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
		
		$post = Post::find($id);
		
		

 $post->delete();




 return Redirect::route('post.index');
	}

}
