<?php

class CommentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
 //$items = DB::table('posts')->where('id', '?');

	// If we get more than one item or no items display an error


	// Extract the first item (which should be the only item)


	//	$Comments = DB::table('comments')->where('postid','?');
	
        //return View::make('pms.view')->with('comments',$Comments)->with('item',$items);
        
        	return View::make('pms.information');
        
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if (Auth::check()) {
		$input = Input::all();
 
 $comment = new Comment();
 $comment->name =  Auth::user()->username;
 $comment->message = $input['message'];
  $comment->postid=$input['postid'];
 $comment->save();
return Redirect::back();
}

else{
	return Redirect::route('user.index');
}


	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$Posts = Post::find($id);
	
	
 $Comments = Comment::where('postid','=',$id)->orderBy('id','DESC')->get();
 
 
        return View::make('pms.view')->with('comments', $Comments)->with('posts',$Posts);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{


	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
			$comment = Comment::find($id);
	$posts=Post::find($id);
 $comment->delete();

  return Redirect::back();
	}





}


