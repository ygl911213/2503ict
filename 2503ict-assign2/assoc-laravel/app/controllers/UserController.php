<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	return View::make('pms.login'); 
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('pms.create'); 
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	 $input = Input::all();
	//	if(!Auth::check()) return Redirect::route('product.index');
$v = Validator::make($input, User::$rules);
if ($v->passes())
 {

$password = $input['password'];
 $encrypted = Hash::make($password);

 $user = new User;
 $user->username = $input['username'];
$user->password = $encrypted;
$user->fullname = $input['fullname'];
$user->birth = $input['birth'];


$user->image = $input['image'];

 $user->save();
 
 
return Redirect::route('post.index'); }
 else{
	return Redirect::action('UserController@create')->withErrors($v);

	
}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	
	
	{
		
			

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$users = User::find($id);
			

			

 		
 return View::make('pms.update')->with('pms', $users);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		
		
		 $input = Input::all();
	//	if(!Auth::check()) return Redirect::route('product.index');

			$user = User::find($id);
		

	
 $input = Input::all();
 
$user->fullname = $input['fullname'];
$user->birth = $input['birth'];
$user->image = $input['image'];

 $user->save();
 
return Redirect::route('post.index', $user->id);
  
 	//print_r($user);
 	

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


public function login()
	{
		


	
		$userdata = array(
			'username' => Input::get('username'),
			'password' => Input:: get('password')
			);
			
		if (Auth::attempt($userdata)){
			
if (Session::has('login_failed'))
{
    Session::forget('login_failed');
}
	

		return Redirect::intended('post');
return Redirect::route('post.index');
		}
		else{
			Session::put('login_failed','Login Failed');

			return Redirect::to(URL::previous())->withInput();
			
		}
		
	}
	
		public function logout()
	{
      Auth::logout();
      return Redirect::action('PostController@index');
}




}