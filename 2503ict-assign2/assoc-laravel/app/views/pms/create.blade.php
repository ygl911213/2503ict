@extends('layouts.master')
<!--create form-->
@section('content')
{{ Form::open(array('url' => secure_url('user') ,'route' => 'user.create', 'files' => true)) }}
{{ Form::label('username', 'User Name: ') }}
{{ Form::text('username') }}
{{$errors->first('username')}}
<p></p>
{{ Form::label('password', 'Password: ') }}
{{ Form::password('password') }}
{{ $errors->first('password') }}
<p></p>
{{ Form::label('fullname', 'fullname: ') }}
{{ Form::text('fullname') }}
{{ $errors->first('fullname') }}
<p></p>
{{ Form::label('birth', 'birth(YYYY-MM-DD): ') }}
{{ Form::text('birth') }}
{{ $errors->first('birth') }}
<p></p>

 {{ Form::label('image', 'Image:') }}
 {{ Form::file('image') }}

<p></p>
{{ Form::submit('Create') }}
{{ Form::close() }}
@stop