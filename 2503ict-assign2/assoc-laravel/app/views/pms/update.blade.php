@extends('layouts.master')

@section('content')
{{Form::model($pms, array('method' => 'PUT', 'route' => array('user.update', $pms->id), 'files' => true));}}

{{ Form::label('fullname', 'fullname: ') }}
{{ Form::text('fullname') }}
{{ $errors->first('fullname') }}
<p></p>
{{ Form::label('birth', 'birth ') }}
{{ Form::text('birth') }}
{{ $errors->first('birth') }}
<p></p>

 {{ Form::label('image', 'Image:') }}
 {{ Form::file('image') }}

<p></p>
{{ Form::submit('update') }}
{{ Form::close() }}
@stop