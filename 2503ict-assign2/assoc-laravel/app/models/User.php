<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class User extends Eloquent implements UserInterface, RemindableInterface,StaplerableInterface {
      public static $rules = array(
      'username' => 'required|unique:users',
      'password' => 'required'
 );
 
 use EloquentTrait;
 
	use UserTrait, RemindableTrait;
 
 
 
 public function __construct(array $attributes = array()) {
 $this->hasAttachedFile('image', [
 'styles' => [
 'medium' => '300x300',
 'thumb' => '100x100'
 ]
 ]);
 parent::__construct($attributes);
 }
 
 

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	

	
	
public function posts()
{
return $this->has_many('Post');
}
 
    public function friends()
    {
        return $this->belongsToMany('User', 'friends_users', 'user_id', 'friend_id');
    }
 
    public function addFriend(User $user)
    {
        $this->friends()->attach($user->id);
    }
 
    public function removeFriend(User $user)
    {
        $this->friends()->detach($user->id);
    }
    
    
public function age()
{


  

    //$interval = $birth_date->diff($now_date); // $interval is a DateInterval
    /* Or, equivalently:
     * $interval = date_diff($birth_date,$now_date);
     */
     $tdate=strtotime(date("Y-m-d"));
     $dob=strtotime($this->birth);
     $age=0;
     while($tdate>$dob =strtotime('+ 1 year',$dob)){
         
         ++$age;
         
     }

    
   
  return $age;
}

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

}



