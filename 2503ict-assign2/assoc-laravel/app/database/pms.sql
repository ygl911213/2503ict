/* Prime Ministers database in SQLite. */
drop table if exists pms;

create table pms (
  id integer primary key autoincrement,
  
  name varchar(40),
  title varchar(40),
  message varchar(100)
  
  );

/* Column names changed to avoid SQLite reserved words. */

insert into pms(name,title,message) values ('Edmund Barton','Good weather','i love sunshine');
insert into pms(name,title,message) values ('Alfred Deakin','Today feeling','i am so happy for today');
insert into pms(name,title,message) values ('Chris Watson','learing','i learn a lot thing today');




CREATE TABLE comment (
	Id INTEGER PRIMARY KEY autoincrement,
	name varchar(40) not null,
	message varchar(100),
postid INTEGER NOT NULL REFERENCES pms(id)
	
);
	
	INSERT INTO comment(name,message,postid)
	VALUES 
	("jack","good",1);
INSERT INTO comment(name,message,postid)
	VALUES 
	("Fred'", "i love it",1);
INSERT INTO comment(name,message,postid)
	VALUES 
	("allen","great",2);
	
	
