<?php
 
class UserSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
 
        User::create(array(
            'username' => 'alexwsears@gmail.com',
            
            'password' => Hash::make('alexsears'),
            'fullname' =>'hello',
            'birth'=>'1991-12-14'
        ));
 
        User::create(array(
            'username' => 'george@foreman.com',
            
            'password' => Hash::make('georgeforeman'),
             'fullname' =>'hell3',
            'birth'=>'1991-12-12'
        ));
 
        User::create(array(
            'username' => 'tony@thetiger.com',
          
            'password' => Hash::make('tonytiger'),
             'fullname' =>'hell213',
            'birth'=>'1921-12-14'
        ));
 
        User::create(array(
            'username' => 'fred@flintstone.com',
       
            'password' => Hash::make('fredflintstone'),
              'fullname' =>'hell21aw3',
            'birth'=>'1921-10-14'
        ));
        
        
        
              User::create(array(
            'username' => 'dianiel@foreman.com',
            
            'password' => Hash::make('george'),
             'fullname' =>'hdianell3',
            'birth'=>'1981-12-12'
        ));
              User::create(array(
            'username' => 'micehal@foroen.com',
            
            'password' => Hash::make('micheal'),
             'fullname' =>'hel12l3',
            'birth'=>'1971-12-12'
        ));
              User::create(array(
            'username' => 'riche@foreman.com',
            
            'password' => Hash::make('irech'),
             'fullname' =>'richiren',
            'birth'=>'1991-12-12'
        ));
              User::create(array(
            'username' => 'superman@foreman.com',
            
            'password' => Hash::make('superman'),
             'fullname' =>'superman',
            'birth'=>'1999-12-12'
        ));
        
    }
 
}