<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */


Route::resource('post', 'PostController');
Route::resource('comment', 'CommentController');
Route::post('user/login', array('as' => 'user.login', 'uses' => 'UserController@login'));
Route::get('user/logout', array('as' => 'user.logout', 'uses' => 'UserController@logout'));
Route::get('friend/search', array('as' => 'friend.search', 'uses' => 'FriendController@search'));
//Route::get('friend/areFriends/{$id}',array('as' => 'friend.areFriends', 'uses' => 'FriendController@areFriends'));
Route::resource('user', 'UserController'); 
Route::resource('friend', 'FriendController'); 
Route::resource('search', 'FriendController');
Route::resource('profile', 'UserController');
Route::resource('update', 'UserController');
//Route::resource('information', 'UserController');
//Route::get('friend/search', array('as' => 'friend.search', 'uses' => 'FriendController@search'));






