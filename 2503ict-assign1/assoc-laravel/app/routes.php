<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */
require "models/pms.php";


// Display search form
Route::get('/', function()
{	
	$results = search();

	
	return View::make('pms.home')->withPms($results);
});


Route::get('information', function()
{	

return View::make('pms.information');
});

Route::get('view/{id}', function($id)
{

 $item = get_item($id);
$comments=comment($id);
  
  	 
  	 return View::make('pms.view')->withComments($comments)->withItem($item);

  
  
	
});




Route::post('add_item_action', function()
{
  $name = Input::get('name');
  $title = Input::get('title');
  $message=input::get('message');
  $id = add_item($name, $title,$message);

  // If successfully created then display newly created item

    return Redirect::to(url(" "));
   
  
});




Route::post('add_comment_action', function()
{
  $name = Input::get('name');
  
  $message=input::get('message');
   $postid=input::get('postid');
  $id = add_comment($name,$message,$postid);

  // If successfully created then display newly created item

    return Redirect::to(url("view/$postid"));
   
  
});

Route::get('edit/{id}', function($id)
{
 $item = get_item($id);
 return View::make('pms.edit')->withItem($item);
});

Route::post('update_item_action', function()
{

$id = Input::get('id');
$message=input::get('message');

update_item($id, $message);

return Redirect::to(url(" "));
});

Route::get('delete_item_action/{id}', function($id)
{
delete_item($id);

return Redirect::to(url(" "));
});

Route::post('delete_comment_action', function()
{
 $itemid = Input::get('item_id');
  
  $comid = input::get('comment_id');
 
delete_comment($comid);

return Redirect::to(url("view/$itemid"));
});





/* Gets item with the given id */


function add_item($name, $title,$message) 
{
  $sql = "insert into pms (name,title,message) values (?,?,?)";

  DB::insert($sql, array($name, $title,$message));

  $id = DB::getPdo()->lastInsertId();

  return $id;
}

function update_item($id, $message)
{
 $sql = "update pms set message = ? where id = ?";
 
 DB::update($sql, array($message, $id));
}

function delete_item($id)
{
 $sql = "delete from pms where id = ?";
 
 DB::delete($sql, array($id));
} 
function add_comment($name,$message,$postid) 
{
  $sql = "insert into comment (name,message,postid) values (?,?,?)";

  DB::insert($sql, array($name,$message,$postid));

  $id = DB::getPdo()->lastInsertId();

  return $id;
}
function delete_comment($id)
{
 $sql = "delete from comment where id = ?";
 
 DB::delete($sql, array($id));
} 

function search() {
  
  $pms = DB::select("select * from pms order by id Desc");
  return $pms;
}

/* Gets item with the given id */
function get_item($id)
{
	$sql = "select id, name, title,message from pms where id = ?";
	$items = DB::select($sql, array($id));

	// If we get more than one item or no items display an error


	// Extract the first item (which should be the only item)
  $item = $items[0];
	return $item;
}

function comment($id) {
  
  $sql = ("select * from comment where postid = ? order by comment.id Desc");
  if($comments=DB::select($sql,array($id))){
  return $comments;
  }else{
  return false;
  }
}
